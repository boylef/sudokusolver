/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.utils;

import com.puzzle.service.parser.sudoku.SudokuParser;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Francis
 */
public class IOUtils {
    
    public static String getFileNameIncludingExtension(String filePath){
        String result = StringUtils.EMPTY_STRING;
        if(filePath!=null){
            int indexOfLastSlash = filePath.lastIndexOf("\\");
            if(indexOfLastSlash<0){
                indexOfLastSlash = filePath.lastIndexOf("/");
            }
            if(indexOfLastSlash>-1){
                result = filePath.substring(indexOfLastSlash+1, filePath.length());
            }else{
                result = filePath;
            }
        }
        return result;
    }
    
    public static String getFileName(String filePath){
        String result = getFileNameIncludingExtension(filePath);
        int indexOfLastDot = result.lastIndexOf(".");
        if(indexOfLastDot>0){
            result = result.substring(0, indexOfLastDot);
        }
        return result;
    }
    
    public static BufferedReader getBufferedReader(String filePath){
        try{
            File file = new File(filePath);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            return bufferedReader;
        }catch(NullPointerException|IOException ex){
            Logger.getLogger(IOUtils.class.getName()).log(Level.SEVERE, "Failed to read file. File not found.", ex); 
        }
        return null;
    } 
    
    public static BufferedWriter getBufferedWriter(String filePath){
        try{
            File file = new File(filePath);
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            return bufferedWriter;
        }catch(NullPointerException|IOException ex){
            Logger.getLogger(IOUtils.class.getName()).log(Level.SEVERE, "Failed to write file. File not found.", ex); 
        }
        return null;
    }
    
    public static boolean closeBufferedReader(BufferedReader bufferedReader){
        if(bufferedReader != null){
            try{
                bufferedReader.close();
            }catch(IOException ex){
                throw new RuntimeException("Failed to close buffered reader.", ex);
            }
        }
        return true;
    }
    
    public static boolean closeBufferedWriter(BufferedWriter bufferedWriter){
        if(bufferedWriter != null){
            try{
                bufferedWriter.close();
            }catch(IOException ex){
                throw new RuntimeException("Failed to close buffered writer.", ex);
            }
        }
        return true;
    }
    
    public static List<File> getAllFilesInFolder(File folder, final String fileExtension){
        List<File> result = new ArrayList<File>();
        if(folder != null  && folder.isDirectory() && StringUtils.isNotNullOrEmpty(fileExtension)){
            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith("." + fileExtension.replaceFirst(".", ""));
                }
            };
            File[] files = folder.listFiles(filter);
            Collections.addAll(result, files);
        }
        return result;
    }
}
