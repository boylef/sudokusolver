/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.utils;

import java.util.List;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Francis
 */
public class CollectionUtils {
    
    public static String getListAsString(final List<Integer> items, String nullValue){
        String result = StringUtils.EMPTY_STRING;
        if(items!=null){
            for(Integer item : items){
                result += item != null ? item.toString() : nullValue;
            }
        }
        return result;
    }
    
    public static boolean hasDuplicates(final List<Integer> datas){
        return hasDuplicates(datas.toArray());
    }
    
    public static boolean hasDuplicates(final Object[] datas){
        boolean result = false;
        Set<Object> dataSet = new HashSet<Object>();
        for (Object data : datas)
        {
          if (data!=null && dataSet.contains(data)){
              result = true;
              break;
          }
          dataSet.add(data);
        }
        return result;
    }
    
    public static Integer getSum(final List<Integer> datas){
        return datas != null ? getSum(datas.toArray()) : null;
    }
    
    public static Integer getSum(final Object[] datas){
        Integer result = 0;
        for (Object data : datas) {
            if (data != null) {
                if (data instanceof Integer){
                    result = result + (Integer)data;
                }
            }
        }
        return result;
    }
}
