/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.dataobject.sudoku;

/**
 *
 * @author Francis
 */
public enum SudokuAlgorithmType implements Displayable{

    BACKTRACKING("BackTracking"), DANCINGLINKS("DancingLinks");

    String display;

    SudokuAlgorithmType(String display) {
        this.display = display;
    }

    @Override
    public String getDisplay() {
        return display;
    }
}
