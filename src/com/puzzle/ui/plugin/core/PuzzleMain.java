/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.ui.plugin.core;

import com.puzzle.ui.sudoku.SudokuSolverControl;
import javax.swing.SwingUtilities;

/**
 *
 * @author Francis
 */
public class PuzzleMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Create and display the form */
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new SudokuSolverControl().setVisible(true);
            }
        });
    }
    
}
