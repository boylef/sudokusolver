/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.service.parser.sudoku;

import com.puzzle.utils.ErrorMessages;
import com.puzzle.dataobject.sudoku.SudokuBoard;
import com.puzzle.utils.CollectionUtils;
import com.puzzle.utils.IOUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Francis
 */
public class SudokuParser {

    
    public SudokuParser(){
        
    }
  
    public SudokuBoard readSudokuFile(BufferedReader bufferedReader){
        SudokuBoard sudokuBoard = new SudokuBoard();
        String line;
        try {
            int row = -1;
            while ((line = bufferedReader.readLine()) != null){
                row++;
                if(row >= SudokuBoard.SUDOKU_BOARD_LENGTH){
                    Logger.getLogger(SudokuParser.class.getName()).log(Level.SEVERE, ErrorMessages.EMSG1, new Exception());
                    return null;
                }
                for (int col = 0; col < line.length(); col++){
                    if(col >= SudokuBoard.SUDOKU_BOARD_LENGTH){
                        Logger.getLogger(SudokuParser.class.getName()).log(Level.SEVERE, ErrorMessages.EMSG4, new Exception());
                        break;
                    }
                    Integer data = null;
                    try{
                        String value = String.valueOf(line.charAt(col));
                        if(!SudokuBoard.NULL_CHAR_VALUE.equals(value)){
                            data = Integer.parseInt(value);
                        }
                    }
                    catch(NumberFormatException ex){
                        Logger.getLogger(SudokuParser.class.getName()).log(Level.WARNING, String.format("Failed to parse character at (%d, %d).", row, col), ex); 
                    }
                    if(data != null && data < 0 && data > SudokuBoard.SUDOKU_BOARD_LENGTH){
                        Logger.getLogger(SudokuParser.class.getName()).log(Level.SEVERE, String.format("Invalid Sudoku Value %d. Must be between 0 and 9.", data), new Exception());
                        data = null;
                    }
                    sudokuBoard.setValue(row, col, data);
                }
                System.out.println(line);
            }
            if(row==-1){
                sudokuBoard = null;
            }
        } catch (IOException ex) {
            Logger.getLogger(SudokuParser.class.getName()).log(Level.SEVERE, ErrorMessages.EMSG3, ex);
        }
        return sudokuBoard;
    }
    
    
}
