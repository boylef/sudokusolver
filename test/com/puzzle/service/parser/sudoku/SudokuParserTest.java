/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.service.parser.sudoku;

import com.puzzle.dataobject.sudoku.SudokuBoard;
import com.puzzle.dataobject.sudoku.SudokuBoardMother;
import com.puzzle.utils.IOUtils;
import com.puzzle.utils.ResourceUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francis
 */
public class SudokuParserTest {
    
    public SudokuParserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of readSudokuFile method, of class SudokuParser.
     */
    @Test
    public void testReadSudokuFile() throws URISyntaxException, FileNotFoundException {
        // Setup fixture
        File file = ResourceUtils.getResourceAsFile(SudokuBoardMother.TEST_PUZZLE_SOLVED_RESOURCE_FILE_PATH);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        SudokuParser instance = new SudokuParser();
        SudokuBoard expResult = SudokuBoardMother.createSolvedBoard();
        // Exercise
        SudokuBoard result = instance.readSudokuFile(bufferedReader);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
}
