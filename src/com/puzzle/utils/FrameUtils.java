/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.utils;

import java.awt.Dimension;
import java.awt.Toolkit;

/**
 *
 * @author Francis
 */
public class FrameUtils {
    public static void centerWindow(javax.swing.JFrame frame){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(screenSize.width/2-frame.getSize().width/2, screenSize.height/2-frame.getSize().height/2);
    }
}
