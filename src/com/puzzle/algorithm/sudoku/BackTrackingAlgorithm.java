/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.algorithm.sudoku;

import com.puzzle.dataobject.sudoku.SudokuBoard;

/**
 *
 * @author Francis
 */
public class BackTrackingAlgorithm implements SudokuSolverAlgorithm{
    
    @Override 
    public SudokuBoard getSolvedPuzzle(SudokuBoard sudokuBoard){
        SudokuBoard result = null;
        if(sudokuBoard!=null){
            result = sudokuBoard.clone();
            solve(result);    
        }
        return result;
    }
    
    private boolean solve(SudokuBoard sudokuBoard){
        for(int row = 0; row < 9; row++){
            for(int col = 0; col < 9; col++){
                if(sudokuBoard.getValue(row, col) == null){
                    for (int num = 1; num <= 9; num++){
                        sudokuBoard.setValue(row, col, num);
                        if(SudokuBoard.isValidNumber(sudokuBoard, row, col) //
                                && solve(sudokuBoard)){
                            return true;
                        }
                        sudokuBoard.setValue(row, col, SudokuBoard.EMPTY_VALUE); 
                    }
                    return false;
                }
            }
        }
        return true;
    }
}
