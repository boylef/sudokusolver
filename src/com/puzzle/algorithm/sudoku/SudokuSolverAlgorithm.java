/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.algorithm.sudoku;

import com.puzzle.dataobject.sudoku.SudokuBoard;

/**
 *
 * @author Francis
 */
public interface SudokuSolverAlgorithm {
    public SudokuBoard getSolvedPuzzle(SudokuBoard sudokuBoard);
}
