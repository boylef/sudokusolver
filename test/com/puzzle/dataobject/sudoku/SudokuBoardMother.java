/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.dataobject.sudoku;

import com.puzzle.service.parser.sudoku.SudokuParser;
import com.puzzle.utils.IOUtils;
import com.puzzle.utils.ResourceUtils;
import java.io.BufferedReader;
import java.io.IOException;

/**
 *
 * @author Francis
 */
public class SudokuBoardMother {
    
    public static final String TEST_PUZZLE_RESOURCE_FILE_PATH = "com/puzzle/service/parser/sudoku/puzzle1.txt";
    public static final String TEST_PUZZLE_TEST_OUTPUT_RESOURCE_FILE_PATH = "com/puzzle/service/parser/sudoku/puzzle1_TestOutput.txt";
    public static final String TEST_PUZZLE_SOLVED_RESOURCE_FILE_PATH = "com/puzzle/service/parser/sudoku/puzzle1.sln.txt";
    
    public static SudokuBoard create(){
        return create(TEST_PUZZLE_RESOURCE_FILE_PATH);
    }
    
    public static SudokuBoard createSolvedBoard(){
        return create(TEST_PUZZLE_SOLVED_RESOURCE_FILE_PATH);
    }
    
    private static SudokuBoard create(String resourcePath){
        BufferedReader bufferedReader = ResourceUtils.getResourceAsBufferedReader(resourcePath);
        SudokuParser sudokuParser = new SudokuParser();
        SudokuBoard result = sudokuParser.readSudokuFile(bufferedReader);
        IOUtils.closeBufferedReader(bufferedReader);
        return result;
    }
}
