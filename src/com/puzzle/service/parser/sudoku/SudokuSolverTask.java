/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.service.parser.sudoku;

import com.puzzle.algorithm.sudoku.BackTrackingAlgorithm;
import com.puzzle.dataobject.sudoku.SudokuBoard;
import com.puzzle.utils.IOUtils;
import com.puzzle.utils.StringUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

/**
 *
 * @author Francis
 */
public class SudokuSolverTask extends SwingWorker<List<SudokuBoard>, String>{

    private final JTextArea textArea;
    private final File directory;
    private final File outputDirectory;
    public SudokuSolverTask(final File directory, final File ouputDirectory, final JTextArea textArea) {
        this.directory = directory;
        this.textArea = textArea;
        this.outputDirectory = ouputDirectory;
    }
    
    @Override
    protected List<SudokuBoard> doInBackground() throws Exception {
        if(directory == null || !directory.isDirectory()){
            publish("SudokuSolverTask cancled. Invalid Input Directory.");
            cancel(true);
        }
        if(outputDirectory == null || !outputDirectory.isDirectory()){
            publish("SudokuSolverTask canceld. Invalid Output Directory.");
            cancel(true);
        }
        textArea.setText(StringUtils.EMPTY_STRING);
        publish("SudokuSolver Start");
        setProgress(0);
        List<SudokuBoard> result = new ArrayList<SudokuBoard>();
        List<File> files = IOUtils.getAllFilesInFolder(directory, SudokuBoard.FILE_EXTENSION);
        int fileIndex = 0;
        int solvedCount = 0;
        int failedCount = 0;
        for(File file : files){
            if (Thread.currentThread().isInterrupted()) {
                throw new InterruptedException("SudokuSolverTask Interrupted While Solving Files.");
            }
            // Load Sudoku Board
            BufferedReader bufferedReader = IOUtils.getBufferedReader(file.getAbsolutePath());
            SudokuParser sudokuParser = new SudokuParser();
            SudokuBoard sudokuBoard = sudokuParser.readSudokuFile(bufferedReader);
            if(sudokuBoard!=null){
                sudokuBoard.setFileName(IOUtils.getFileName(file.getName()));
                // Solve Sudoku Board
                BackTrackingAlgorithm backTrackingAlgorithm = new BackTrackingAlgorithm();
                Long startTime = System.nanoTime();
                SudokuBoard solvedSudokuBoard = backTrackingAlgorithm.getSolvedPuzzle(sudokuBoard);
                double elapsedTime = System.nanoTime()-startTime;
                if(solvedSudokuBoard != null && solvedSudokuBoard.isSolved()){
                    solvedCount++;
                    result.add(solvedSudokuBoard);
                    publish(String.format("Successfully solved sudoku puzzle \"%s\". Total time taken %.8f seconds.", sudokuBoard.getFileName(), (double)(elapsedTime / 1000000000.0)));
                    // Export the solved puzzle to the output folder
                    String outputFilePath = outputDirectory.getAbsolutePath() + "\\" + sudokuBoard.getFileName() + ".sln" + SudokuBoard.FILE_EXTENSION;
                    SudokuExporter sudokuExporter = new SudokuExporter();
                    BufferedWriter bufferedWriter = IOUtils.getBufferedWriter(outputFilePath);
                    sudokuExporter.exportSudokuFile(solvedSudokuBoard, bufferedWriter);
                    IOUtils.closeBufferedWriter(bufferedWriter);
                    publish("Sucessfully exported solved puzzle.");
                }else{
                    publish(String.format("Failed to solve sudoku file \"%s\".", file.getName()));
                    failedCount++;
                }
            }else{
                publish(String.format("Failed to import sudoku file \"%s\".", file.getName()));
                failedCount++;
            }
            setProgress((++fileIndex) * 100 / files.size());
        }
        setProgress(100);
        publish(String.format("SudokuSolver Finsihed. Total succeeded %d. Total failed %d.", solvedCount, failedCount));
        return result;
    }
    
    @Override
    protected void process(List<String> chunks) {
      for (final String string : chunks) {
        textArea.append(string);
        textArea.append("\n");
      }
    }
    
}
