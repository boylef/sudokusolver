/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.utils;

/**
 *
 * @author Francis
 */
public class ErrorMessages {
    public static final String EMSG1 = "Invalid Sudoku File. The number of rows must be less than 9.";
    public static final String EMSG2 = "Invalid Sudoku File. Duplicate values in rows are not permitted.";
    public static final String EMSG3 = "Failed to read Sudoku file.";
    public static final String EMSG4 = "Incorrect Sudoku File Format. Rows can not exceed 9 characters.";
}
