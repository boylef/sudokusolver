/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.dataobject.sudoku;

import com.puzzle.utils.StringUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francis
 */
public class SudokuBoardTest {
    
    SudokuBoard sudokuBoard;
    private static SudokuBoard puzzle1SudokuBoard;
    private static SudokuBoard solvedSudokuBoard;
    public SudokuBoardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        solvedSudokuBoard = SudokuBoardMother.createSolvedBoard();
        puzzle1SudokuBoard = SudokuBoardMother.create();
    }
    
    @AfterClass
    public static void tearDownClass() {
        solvedSudokuBoard = null;
        puzzle1SudokuBoard = null;
    }
    
    @Before
    public void setUp() {
        sudokuBoard = puzzle1SudokuBoard.clone();
    }
    
    @After
    public void tearDown() {
        sudokuBoard = null;
    }

    private void assertEqualsSudokuBoard(SudokuBoard expected, SudokuBoard actual){
        assertEquals(expected, actual);
        assertEquals(expected.getFileName(), actual.getFileName());
    }
    
    private void assertEqualsList(Object[] expected, Object[] actual){
        assertEquals(expected.length, actual.length);
        for(int i = 0; i < expected.length; i++){
            assertEquals(expected[i], actual[i]);
        }
    }
    
    /**
     * Test of clone method, of class SudokuBoard.
     */
    @Test
    public void testClone() {
        // Setup fxiture
        // Exercise
        SudokuBoard result = sudokuBoard.clone();
        // Verify outcome
        assertEqualsSudokuBoard(sudokuBoard, result);
    }

    /**
     * Test of isSolved method, of class SudokuBoard.
     */
    @Test
    public void testIsSolved() {
        // Setup fixture
        boolean expResult = true;
        // Exercise
        boolean result = solvedSudokuBoard.isSolved();
        // Verify outcome
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isSolved method, of class SudokuBoard.
     */
    @Test
    public void testIsSolved_TESTWithUnsolvedBoard() {
        // Setup fixture
        boolean expResult = false;
        // Exercise
        boolean result = sudokuBoard.isSolved();
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of setValue method, of class SudokuBoard.
     */
    @Test
    public void testSetValue() {
        // Setup fixture
        int row = 1;
        int col = 2;
        Integer oldValue = sudokuBoard.getValue(row, col);
        Integer expected = 2;
        // Exercise
        sudokuBoard.setValue(row, col, expected);
        // Verify outcome
        Integer actual = sudokuBoard.getValue(row, col);
        assertNotEquals(oldValue, expected);
        assertEquals(expected, actual);
    }

    /**
     * Test of getValue method, of class SudokuBoard.
     */
    @Test
    public void testGetValue() {
        // Setup Fixture
        int row = 1;
        int col = 2;
        Integer expResult = 6;
        // Exercise
        Integer result = sudokuBoard.getValue(row, col);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of getRow method, of class SudokuBoard.
     */
    @Test
    public void testGetRow() {
        // Setup fixture
        int row = 0;
        List<Integer> expResult = new ArrayList<Integer>();
        Collections.addAll(expResult, null, null, null, 1, 5, null, null, 7, null);
        // Exercise
        List<Integer> result = sudokuBoard.getRow(row);
        // Verify outcome
        assertNotNull(result);
        assertEqualsList(expResult.toArray(), result.toArray());
    }

    /**
     * Test of getColumn method, of class SudokuBoard.
     */
    @Test
    public void testGetColumn() {
        // Setup fixture
        int col = 0;
        List<Integer> expResult = new ArrayList<Integer>();
        Collections.addAll(expResult, null, 1, 3, 9, null, 7, null, null, null);
        // Exercise
        List<Integer> result = sudokuBoard.getColumn(col);
        // Verify outcome
        assertNotNull(result);
        assertEqualsList(expResult.toArray(), result.toArray());
    }

    /**
     * Test of getSudokuCells method, of class SudokuBoard.
     */
    @Test
    public void testGetSudokuCells() {
        // Setup fixture
        // Exercise
        List<List<Integer>> result = sudokuBoard.getSudokuCells();
        // Verify outcome
        assertNotNull(result);
        assertEquals(result.size(), (int)SudokuBoard.SUDOKU_BOARD_LENGTH);
        for(List<Integer> cell : result){
            assertNotNull(cell);
            assertEquals(cell.size(), (int)SudokuBoard.SUDOKU_BOARD_LENGTH);
        }
    }

    /**
     * Test of setFileName method, of class SudokuBoard.
     */
    @Test
    public void testSetFileName() {
        // Setup fixture
        String expected = "TestSetFileName";
        // Exercise
        sudokuBoard.setFileName(expected);
        // Verify outcome
        String actual = sudokuBoard.getFileName();
        assertEquals(expected, actual);
    }

    /**
     * Test of getFileName method, of class SudokuBoard.
     */
    @Test
    public void testGetFileName() {
        // Setup fixture
        String expected = "TestGetFileName";
        sudokuBoard.setFileName(expected);
        // Exercise
        String actual = sudokuBoard.getFileName();
        // Verify outcome
        assertEquals(expected, actual);
    }

    /**
     * Test of toString method, of class SudokuBoard.
     */
    @Test
    public void testToString() {
        // Setup fixture
        String expResult = "XXX15XX7X" + StringUtils.NEW_LINE + //
                           "1X6XXX82X" + StringUtils.NEW_LINE + //
                           "3XX86XX4X" + StringUtils.NEW_LINE + //
                           "9XX4XX567" + StringUtils.NEW_LINE + //
                           "XX47X83XX" + StringUtils.NEW_LINE + //
                           "732XX6XX4" + StringUtils.NEW_LINE + //
                           "X4XX81XX9" + StringUtils.NEW_LINE + //
                           "X17XXX2X8" + StringUtils.NEW_LINE + //
                           "X5XX37XXX";
        // Exercise
        String result = sudokuBoard.toString();
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class SudokuBoard.
     */
    @Test
    public void testEquals() {
        // Setup fixture
        Object obj = null;
        boolean expResult = true;
        // Exercise
        boolean result = sudokuBoard.equals(sudokuBoard.clone());
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidSudokuCell method, of class SudokuBoard.
     */
    @Test
    public void testIsValidSudokuCell() {
        // Setup fixture
        int cellIndex = 0;
        boolean expResult = true;
        // Exercise
        boolean result = SudokuBoard.isValidSudokuCell(sudokuBoard, cellIndex);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
        /**
     * Test of isValidSudokuCell method, of class SudokuBoard.
     */
    @Test
    public void testIsValidSudokuCell_TESTWithDuplicateValueInFirstCell() {
        // Setup fixture
        int cellIndex = 0;
        sudokuBoard.setValue(0,0,1);
        boolean expResult = false;
        // Exercise
        boolean result = SudokuBoard.isValidSudokuCell(sudokuBoard, cellIndex);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of getCellIndex method, of class SudokuBoard.
     */
    @Test
    public void testGetCellIndex() {
        int row = 3;
        int col = 0;
        int expResult = 3;
        // Exercise
        int result = SudokuBoard.getCellIndex(row, col);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidNumber method, of class SudokuBoard.
     */
    @Test
    public void testIsValidNumber() {
        // Setup fixture
        int row = 0;
        int col = 0;
        int number = 2;
        sudokuBoard.setValue(row, col, number);
        boolean expResult = true;
        // Exercise
        boolean result = SudokuBoard.isValidNumber(sudokuBoard, row, col);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
        /**
     * Test of isValidNumber method, of class SudokuBoard.
     */
    @Test
    public void testIsValidNumber_TESTWithInvalidNumber() {
        // Setup fixture
        int row = 0;
        int col = 0;
        int number = 1;
        sudokuBoard.setValue(row, col, number);
        boolean expResult = false;
        // Exercise
        boolean result = SudokuBoard.isValidNumber(sudokuBoard, row, col);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidData method, of class SudokuBoard.
     */
    @Test
    public void testIsValidData() {
        // Setup fixture
        List<Integer> data = new ArrayList<Integer>();
        Collections.addAll(data, null, null, 1, null, 4, 6, null, null, 2);
        boolean expResult = true;
        // Exercise
        boolean result = SudokuBoard.isValidData(data);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
        /**
     * Test of isValidData method, of class SudokuBoard.
     */
    @Test
    public void testIsValidData_TESTWithInvalidData() {
        // Setup fixture
        List<Integer> data = new ArrayList<Integer>();
        Collections.addAll(data, null, null, 1, 2, 4, 6, null, null, 2);
        boolean expResult = false;
        // Exercise
        boolean result = SudokuBoard.isValidData(data);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidColumn method, of class SudokuBoard.
     */
    @Test
    public void testIsValidColumn() {
        // Setup fixture
        int col = 0;
        boolean expResult = true;
        // Exercise
        boolean result = SudokuBoard.isValidColumn(sudokuBoard, col);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidColumn method, of class SudokuBoard.
     */
    @Test
    public void testIsValidColumn_TESTWithInvalidColumn() {
        // Setup fixture
        int col = 0;
        boolean expResult = false;
        sudokuBoard.setValue(0, 0, 1);
        // Exercise
        boolean result = SudokuBoard.isValidColumn(sudokuBoard, col);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidRow method, of class SudokuBoard.
     */
    @Test
    public void testIsValidRow() {
        // Setup fixture
        int row = 0;
        boolean expResult = true;
        // Exercise
        boolean result = SudokuBoard.isValidRow(sudokuBoard, row);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidRow method, of class SudokuBoard.
     */
    @Test
    public void testIsValidRow_TESTWithInvalidRow() {
        // Setup fixture
        int row = 0;
        sudokuBoard.setValue(0,0,1);
        boolean expResult = false;
        // Exercise
        boolean result = SudokuBoard.isValidRow(sudokuBoard, row);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidSudokuBoard method, of class SudokuBoard.
     */
    @Test
    public void testIsValidSudokuBoard() {
        // Setup fixture
        boolean expResult = true;
        // Exercise
        boolean result = SudokuBoard.isValidSudokuBoard(sudokuBoard);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidSudokuBoard method, of class SudokuBoard.
     */
    @Test
    public void testIsValidSudokuBoard_TESTWithInvalidBoard() {
        // Setup fixture
        sudokuBoard.setValue(0,0,1);
        boolean expResult = false;
        // Exercise
        boolean result = SudokuBoard.isValidSudokuBoard(sudokuBoard);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
}
