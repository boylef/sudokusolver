/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.utils;

import com.puzzle.dataobject.sudoku.SudokuBoardMother;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francis
 */
public class IOUtilsTest {
    
    public IOUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFileNameIncludingExtension method, of class IOUtils.
     */
    @Test
    public void testGetFileNameIncludingExtension() {
        // Setup fixture
        String filePath = "Q:\\Users\\Public\\test.txt";
        String expResult = "test.txt";
        // Exercise
        String result = IOUtils.getFileNameIncludingExtension(filePath);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getFileNameIncludingExtension method, of class IOUtils.
     */
    @Test
    public void testGetFileNameIncludingExtension_TESTWithForwardSlash() {
        // Setup fixture
        String filePath = "/Users/Public/test.txt";
        String expResult = "test.txt";
        // Exercise
        String result = IOUtils.getFileNameIncludingExtension(filePath);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getFileNameIncludingExtension method, of class IOUtils.
     */
    @Test
    public void testGetFileNameIncludingExtension_TESTWithNull() {
        // Setup fixture
        String expResult = StringUtils.EMPTY_STRING;
        // Exercise
        String result = IOUtils.getFileNameIncludingExtension(null);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of getFileName method, of class IOUtils.
     */
    @Test
    public void testGetFileName() {
        // Setup fixture
        String filePath = "Q:\\Users\\Public\\test.txt";
        String expResult = "test";
        // Exercise
        String result = IOUtils.getFileName(filePath);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
        /**
     * Test of getFileName method, of class IOUtils.
     */
    @Test
    public void testGetFileName_TESTWithNull() {
        // Setup fixture
        String expResult = StringUtils.EMPTY_STRING;
        // Exercise
        String result = IOUtils.getFileName(null);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of getBufferedReader method, of class IOUtils.
     */
    @Test
    public void testGetBufferedReader() throws IOException {
        // Setup fixture
        String filePath = new File(StringUtils.EMPTY_STRING).getAbsolutePath()+"\\test\\com\\puzzle\\service\\parser\\sudoku\\puzzle1.txt";
        // Exercise
        BufferedReader result = IOUtils.getBufferedReader(filePath);
        // Verify outcome
        assertNotNull(result);
        result.close();
    }
    
    /**
     * Test of getBufferedReader method, of class IOUtils.
     */
    @Test
    public void testGetBufferedReader_TESTWithInvalidFilePath() throws IOException {
        // Setup fixture
        String filePath = StringUtils.EMPTY_STRING;
        // Exercise
        BufferedReader result = IOUtils.getBufferedReader(filePath);
        // Verify outcome
        assertNull(result);
        if(result!=null)
        result.close();
    }
    
        /**
     * Test of getBufferedReader method, of class IOUtils.
     */
    @Test
    public void testGetBufferedReader_TESTWithNull() throws IOException {
        // Setup fixture
        // Exercise
        BufferedReader result = IOUtils.getBufferedReader(null);
        // Verify outcome
        assertNull(result);
        if(result!=null)
        result.close();
    }

    /**
     * Test of getBufferedWriter method, of class IOUtils.
     */
    @Test
    public void testGetBufferedWriter() throws IOException{
        // Setup fixture
        String filePath = new File(StringUtils.EMPTY_STRING).getAbsolutePath()+"\\test\\com\\puzzle\\service\\parser\\sudoku\\puzzle1.txt";
        // Exercise
        BufferedWriter result = IOUtils.getBufferedWriter(filePath);
        // Verify outcome
        assertNotNull(result);
        result.close();
    }
    
    /**
     * Test of getBufferedWriter method, of class IOUtils.
     */
    @Test
    public void testGetBufferedWriter_TESTWithInvalidFilePath() throws IOException{
        // Setup fixture
        String filePath = StringUtils.EMPTY_STRING;
        // Exercise
        BufferedWriter result = IOUtils.getBufferedWriter(filePath);
        // Verify outcome
        assertNull(result);
        if(result!=null)
        result.close();
    }
    
    /**
     * Test of getBufferedWriter method, of class IOUtils.
     */
    @Test
    public void testGetBufferedWriter_TESTWithNull() throws IOException{
        // Setup fixture
        // Exercise
        BufferedWriter result = IOUtils.getBufferedWriter(null);
        // Verify outcome
        assertNull(result);
        if(result!=null)
        result.close();
    }

    /**
     * Test of closeBufferedReader method, of class IOUtils.
     */
    @Test
    public void testCloseBufferedReader() {
        // Setup fixture
        BufferedReader bufferedReader = createBufferedReader();
        boolean expResult = true;
        // Exercise
        boolean result = IOUtils.closeBufferedReader(bufferedReader);
        // Verify outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of closeBufferedWriter method, of class IOUtils.
     */
    @Test
    public void testCloseBufferedWriter() {
        // Setup fixture
        BufferedWriter bufferedWriter = createBufferedWriter();
        boolean expResult = true;
        // Exercise
        boolean result = IOUtils.closeBufferedWriter(bufferedWriter);
        // Verify outcome
        assertEquals(expResult, result);
    }
    
        /**
     * Test of getAllFilesInFolder method, of class IOUtils.
     */
    @Test
    public void testGetAllFilesInFolder() {
        // Setup fixture
        String fileExtension = ".txt";
        File file = ResourceUtils.getResourceAsFile(SudokuBoardMother.TEST_PUZZLE_RESOURCE_FILE_PATH);
        File folder = file.getParentFile();
        // Exercise
        List<File> result = IOUtils.getAllFilesInFolder(folder, fileExtension);
        // Verify outcome
        assertNotNull(result);
        assertEquals(result.size(), 7);
        assertTrue(result.get(0).getAbsolutePath().endsWith(fileExtension));
    }
    
    private BufferedReader createBufferedReader(){
        File file = ResourceUtils.getResourceAsFile(SudokuBoardMother.TEST_PUZZLE_RESOURCE_FILE_PATH);
        return IOUtils.getBufferedReader(file.getAbsolutePath());
    }
    
    private BufferedWriter createBufferedWriter(){
        File file = ResourceUtils.getResourceAsFile(SudokuBoardMother.TEST_PUZZLE_RESOURCE_FILE_PATH);
        return IOUtils.getBufferedWriter(file.getAbsolutePath());
    }
    
    
    
}
