/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.utils;

import com.puzzle.dataobject.sudoku.SudokuBoard;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francis
 */
public class CollectionUtilsTest {
    
    public CollectionUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListAsString method, of class CollectionUtils.
     */
    @Test
    public void testGetListAsString() {
        // Setup fixture
        List<Integer> items = new ArrayList<Integer>();
        Collections.addAll(items, 1, 2, 3);
        String expResult = "123";
        // Exercise
        String result = CollectionUtils.getListAsString(items, SudokuBoard.NULL_CHAR_VALUE);
        // Verify Outcome
        assertEquals(expResult, result);
    }
    
        /**
     * Test of getListAsString method, of class CollectionUtils.
     */
    @Test
    public void testGetListAsString_TESTWithNull() {
        // Setup fixture
        String expResult = StringUtils.EMPTY_STRING;
        // Exercise
        String result = CollectionUtils.getListAsString(null, SudokuBoard.NULL_CHAR_VALUE);
        // Verify Outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of hasDuplicates method, of class CollectionUtils.
     */
    @Test
    public void testHasDuplicates_List() {
        // Setup fixture
        List<Integer> datas = new ArrayList<Integer>();
        Collections.addAll(datas, 1, 2, 3);
        boolean expResult = false;
        // Exercise
        boolean result = CollectionUtils.hasDuplicates(datas);
        // Verify Outcome
        assertEquals(expResult, result);
    }
    /**
     * Test of hasDuplicates method, of class CollectionUtils.
     */
    @Test
    public void testHasDuplicates_List_TESTWithDuplicates() {
        // Setup fixture
        List<Integer> datas = new ArrayList<Integer>();
        Collections.addAll(datas, 1, 2, 3, 5, 2, 1);
        boolean expResult = true;
        // Exercise
        boolean result = CollectionUtils.hasDuplicates(datas);
        // Verify Outcome
        assertEquals(expResult, result);
    }

    /**
     * Test of getSum method, of class CollectionUtils.
     */
    @Test
    public void testGetSum_List() {
        // Setup fixture
        List<Integer> datas = new ArrayList<Integer>();
        Collections.addAll(datas, 1, 2, 3);
        Integer expResult = 6;
        // Exercise
        Integer result = CollectionUtils.getSum(datas);
        // Verify Outcome
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSum method, of class CollectionUtils.
     */
    @Test
    public void testGetSum_List_TESTWithNull() {
        // Setup fixture
        List<Integer> datas = null;
        Integer expResult = null;
        // Exercise
        Integer result = CollectionUtils.getSum(datas);
        // Verify Outcome
        assertEquals(expResult, result);
    }
}
