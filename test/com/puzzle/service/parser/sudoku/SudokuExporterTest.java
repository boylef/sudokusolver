/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.service.parser.sudoku;

import com.puzzle.dataobject.sudoku.SudokuBoard;
import com.puzzle.dataobject.sudoku.SudokuBoardMother;
import com.puzzle.utils.ResourceUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francis
 */
public class SudokuExporterTest {
    
    private static SudokuBoard sudokuBoard;
    public SudokuExporterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        sudokuBoard = SudokuBoardMother.create();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of exportSudokuFile method, of class SudokuExporter.
     */
    @Test
    public void testExportSudokuFile() throws URISyntaxException, IOException {
        // Setup fixture
        File file = ResourceUtils.getResourceAsFile(SudokuBoardMother.TEST_PUZZLE_TEST_OUTPUT_RESOURCE_FILE_PATH);
        file.delete();
        assertFalse(file.exists());
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        SudokuExporter instance = new SudokuExporter();
        // Execute
        instance.exportSudokuFile(sudokuBoard, bufferedWriter);
        // Verify outcome
        assertTrue(file.exists());
    }
    
}
