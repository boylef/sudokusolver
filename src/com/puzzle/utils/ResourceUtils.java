/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.utils;

import java.awt.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Francis
 */
public class ResourceUtils {
    
    public static BufferedReader getResourceAsBufferedReader(String resourcePath){
        BufferedReader result = null;
        InputStream inputStream = getResourceAsStream(resourcePath);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        result = new BufferedReader(inputStreamReader);
        return result;
    }
    
    public static InputStream getResourceAsStream(String resourcePath){
        InputStream result = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if(classLoader!=null){
            result = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath);
        }
        return result;
    }
    
    public static File getResourceAsFile(String resourcePath){
        URL url = ResourceUtils.getResourceURL(resourcePath);
        File result = null;
        try {
            result = Paths.get(url.toURI()).toFile();
        } catch (URISyntaxException ex) {
            Logger.getLogger(ResourceUtils.class.getName()).log(Level.SEVERE, "Failed to load resource.", ex);
        }
        return result;
    }
    
    public static Image getResourceAsImage(String resourcePath){
        Image result = null;
        InputStream inputStream = getResourceAsStream(resourcePath);
        if(inputStream!=null){
            try {
                result = ImageIO.read(inputStream);
            } catch (IOException ex) {
                Logger.getLogger(ResourceUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }
    
    public static URL getResourceURL(String resourcePath){
        URL result = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if(classLoader!=null){
            result = Thread.currentThread().getContextClassLoader().getResource(resourcePath);
        }
        return result;
    }
}
