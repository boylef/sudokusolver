/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.algorithm.sudoku;

import com.puzzle.dataobject.sudoku.SudokuBoard;
import com.puzzle.dataobject.sudoku.SudokuBoardMother;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Francis
 */
public class BackTrackingAlgorithmTest {
    
    SudokuBoard sudokuBoard;
    SudokuBoard solvedSudokuBoard;
    BackTrackingAlgorithm sut;
    public BackTrackingAlgorithmTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        sut = new BackTrackingAlgorithm();
        sudokuBoard = SudokuBoardMother.create();
        solvedSudokuBoard = SudokuBoardMother.createSolvedBoard();
    }
    
    @After
    public void tearDown() {
        sut = null;
        sudokuBoard = null;
    }

    /**
     * Test of getSolvedPuzzle method, of class BackTrackingAlgorithm.
     */
    @Test
    public void testGetSolvedPuzzle() {
        // Setup fixture
        BackTrackingAlgorithm instance = new BackTrackingAlgorithm();
        SudokuBoard expResult = this.solvedSudokuBoard;
        // Exercise 
        SudokuBoard result = instance.getSolvedPuzzle(this.sudokuBoard);
        // Verifiy outcome
        assertNotNull(result);
        assertEquals(expResult, result);
        assertTrue(result.isSolved());
    }
    
}
