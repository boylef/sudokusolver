# SudokuSolver

Example Sudoku Solver. This application will perform a Batch solve for text sudoku puzzles using the BackTracking Algorithm and export the solved puzzle files to a selected output directory.
The project uses JUnit for unit testing and the TikiOne JaCoCoverage plugin for code coverage.