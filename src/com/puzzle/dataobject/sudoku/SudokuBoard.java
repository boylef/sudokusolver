/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.dataobject.sudoku;

import com.puzzle.utils.CollectionUtils;
import com.puzzle.utils.StringUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 *
 * @author Francis
 */
public class SudokuBoard {
    public static final String FILE_EXTENSION = ".txt";
    public static final String NULL_CHAR_VALUE = "X";
    public static final Integer EMPTY_VALUE = null;
    public static final Integer SUDOKU_BOARD_LENGTH = 9;
    List<List<Integer>> sudokuDataTable = new ArrayList<List<Integer>>();
    private String fileName = "SudokuPuzzle.txt";
    

    public SudokuBoard(){
        this.sudokuDataTable = createEmptySudokuBoard();
    }
    
    public SudokuBoard clone(){
        SudokuBoard sudokuBoard = new SudokuBoard();
        for(int row = 0; row < SUDOKU_BOARD_LENGTH; row++){
            for (int col = 0; col < SUDOKU_BOARD_LENGTH; col++){
                sudokuBoard.setValue(row, col, getValue(row, col));
            }
        }
        return sudokuBoard;
    }
    
    private List<List<Integer>> createEmptySudokuBoard(){
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        for(int row = 0; row < 9; row ++){
            List<Integer> dataRow = new ArrayList<Integer>();
            for (int col = 0; col < 9; col++){
                dataRow.add(EMPTY_VALUE);
            }
            result.add(dataRow);
        }
        return result;
    }
    
    
    private boolean isComplete(){
        for(List<Integer> dataRow : sudokuDataTable){
            if(dataRow.contains(null)){
                return false;
            }
        }
        return true;
    }
    
    public boolean isSolved(){
        return isComplete() && isValidSudokuBoard(this);
    }
    
    private boolean isValidBoardSize(){
        boolean result = true;
        if (sudokuDataTable == null || sudokuDataTable.size() != SUDOKU_BOARD_LENGTH){
            result = false;
        }
        for(List<Integer> dataRow : sudokuDataTable){
            if(dataRow == null || dataRow.size() != SUDOKU_BOARD_LENGTH){
                result = false;
            }
        }
        return result;
    }
    
    private boolean isValidIndex(int row, int col){
        return isValidBoardSize() && row >= 0 && row < SUDOKU_BOARD_LENGTH && col >= 0 && col < SUDOKU_BOARD_LENGTH;
    }
    
    public void setValue(int row, int col, Integer value){
        if(isValidIndex(row, col)){
            sudokuDataTable.get(row).set(col, value);
        }
    }
    
    public Integer getValue(int row, int col){
        Integer result = null;
        if(isValidIndex(row, col)){
            result = sudokuDataTable.get(row).get(col);
        }
        return result;
    }
    
    public List<Integer> getRow(int row){
        List<Integer> result = new ArrayList<Integer>();
        if(isValidIndex(row,0)){
            result = sudokuDataTable.get(row);
        }
        return Collections.unmodifiableList(result);
    }
    
    public List<Integer> getColumn(int col){
        List<Integer> result = new ArrayList<Integer>();
        if(isValidIndex(0,col)){
            for(int row = 0; row < sudokuDataTable.size(); row++){
                result.add(sudokuDataTable.get(row).get(col));
            }
        }
        return Collections.unmodifiableList(result);
    }
    
    public List<List<Integer>> getSudokuCells(){
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        for(int i = 0; i < SUDOKU_BOARD_LENGTH; i++){
            result.add(new ArrayList<Integer>());
        }
        int cellRowIndex = -1;
        for(int row = 0; row < sudokuDataTable.size(); row++){
            if(row % 3 == 0){
                cellRowIndex++;
            }
            int cellColIndex = -1;
            for(int col = 0; col < sudokuDataTable.get(row).size(); col++){
                if(col % 3 == 0){
                    cellColIndex++;
                }
                Integer data  = sudokuDataTable.get(row).get(col);
                result.get(cellRowIndex*3+cellColIndex).add(data);
            }
        }
        return Collections.unmodifiableList(result);
    }
    
    public void setFileName(String fileName){
        this.fileName = fileName;
    }
    public String getFileName(){
        return fileName;
    }

    @Override
    public String toString() {
        String result = StringUtils.EMPTY_STRING;
        for(List<Integer> dataRow : sudokuDataTable){
            result += CollectionUtils.getListAsString(dataRow, NULL_CHAR_VALUE);
            if(!dataRow.equals(sudokuDataTable.get(sudokuDataTable.size()-1))){
                result += StringUtils.NEW_LINE;
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof SudokuBoard ? this.toString().equals(obj.toString()) : super.equals(obj);
    }
    
    public static boolean isValidSudokuCell(SudokuBoard sudokuBoard, int cellIndex){
        boolean result = true;
        if(cellIndex < 0 || cellIndex > 8){
            return false;
        }
        List<List<Integer>> cells = sudokuBoard.getSudokuCells();
        if(cells.size() != 9 || !isValidData(cells.get(cellIndex))){
            result = false;
        }
        return result;
    }
    
    public static int getCellIndex(int row, int col){
        return (row / 3) * 3 + (col / 3);
    }
    
    public static boolean isValidNumber(SudokuBoard sudokuBoard, int row, int col){
        int cellIndex = getCellIndex(row, col);
        boolean result = isValidRow(sudokuBoard, row) //
                && isValidColumn(sudokuBoard, col) //
                && isValidSudokuCell(sudokuBoard, cellIndex);
        return result;
    }
    
    public static boolean isValidData(List<Integer> data){
        boolean result = true;
        if(data == null || data.size() != SudokuBoard.SUDOKU_BOARD_LENGTH || CollectionUtils.hasDuplicates(data)){
            result = false;
        }
        return result;
    }
    public static boolean isValidColumn(SudokuBoard sudokuBoard, int col){
        List<Integer> dataCol = sudokuBoard.getColumn(col);
        return isValidData(dataCol);
    }
    
    public static boolean isValidRow(SudokuBoard sudokuBoard, int row){
        List<Integer> dataRow = sudokuBoard.getRow(row);
        return isValidData(dataRow);
    }
    
    public static boolean isValidSudokuBoard(SudokuBoard sudokuBoard){
        for (int row = 0; row < SudokuBoard.SUDOKU_BOARD_LENGTH; row++){
            if(!isValidRow(sudokuBoard, row)){
                return false;
            }
        }
        for (int col = 0; col < SudokuBoard.SUDOKU_BOARD_LENGTH; col++){
            if(!isValidColumn(sudokuBoard, col)){
                return false;
            }
        }
        List<List<Integer>> cells = sudokuBoard.getSudokuCells();
        if(cells == null || cells.size() != SudokuBoard.SUDOKU_BOARD_LENGTH){
            return false;
        }
        for(int cellIndex = 0; cellIndex < cells.size(); cellIndex++){
            if(!isValidData(cells.get(cellIndex))){
                return false;
            }
        }
        return true;
    }
    
    
    
}
