/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puzzle.service.parser.sudoku;

import com.puzzle.dataobject.sudoku.SudokuBoard;
import com.puzzle.utils.IOUtils;
import java.io.BufferedWriter ;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Francis
 */
public class SudokuExporter {
    public void exportSudokuFile(SudokuBoard sudokuBoard, BufferedWriter bufferedWriter){
        String outputFile = sudokuBoard.toString();
        try {
            bufferedWriter.write(outputFile);
        } catch (IOException ex) {
            Logger.getLogger(SudokuExporter.class.getName()).log(Level.SEVERE, String.format("Failed to export file \"%s\".", sudokuBoard.getFileName()), ex);
        }
        IOUtils.closeBufferedWriter(bufferedWriter);        
    }
}
